﻿// Stock Price Receiver
// Student: 15007561
using System;
using System.Threading;
using System.Windows.Forms;

namespace Stock_Price_Receiver
{
    public partial class FormMain : Form
    {
        delegate void UpdateStockDelegate(StockDetails stock);
        DataReceiver dataReceiver = new DataReceiver();
        Thread listeningThread = null;

        public FormMain()
        {
            InitializeComponent();

            // Add stocks in order of their stock IDs so we can use the ID as the index to update the table.
            dgvStockPrices.Rows.Add(new object[] { "BT Plc", "0", "0" });               // 0
            dgvStockPrices.Rows.Add(new object[] { "HSBC holdings Plc", "0", "0" });    // 1
            dgvStockPrices.Rows.Add(new object[] { "Admiral Group", "0", "0" });        // 2
            dgvStockPrices.Rows.Add(new object[] { "BAE Systems Plc", "0", "0" });      // 3 
            dgvStockPrices.Rows.Add(new object[] { "Intertek Group Plc", "0", "0" });   // 4

            // Create main menu to select which stocks to show
            Menu = new MainMenu();
            MenuItem mnuSelectData = Menu.MenuItems.Add("Select Data");
            mnuSelectData.MenuItems.Add("Show All", menuItem_Click);
            mnuSelectData.MenuItems.Add("BT Plc", menuItem_Click);
            mnuSelectData.MenuItems.Add("HSBC holdings Plc", menuItem_Click);
            mnuSelectData.MenuItems.Add("Admiral Group", menuItem_Click);
            mnuSelectData.MenuItems.Add("BAE Systems Plc", menuItem_Click);
            mnuSelectData.MenuItems.Add("Intertek Group Plc", menuItem_Click);
        }

        private void menuItem_Click(object sender, EventArgs e)
        {
            MenuItem item = sender as MenuItem;

            // Selected "Show All"
            if (item.Index == 0)
            {
                toggleStockVisible(true);
            }
            else // Selected individual stock
            {
                toggleStockVisible(false);
                dgvStockPrices.Rows[item.Index - 1].Visible = true;
            }
        }

        /// <summary>
        /// Toggle the visibility of the all stocks in the data grid view.
        /// </summary>
        /// <param name="visible">true = all visible, false = hide all</param>
        private void toggleStockVisible(bool visible)
        {
            foreach (DataGridViewRow row in dgvStockPrices.Rows)
            {
                if (!row.IsNewRow) // skip the empty new row
                {
                    if (visible)
                        row.Visible = true;
                    else
                        row.Visible = false;
                }
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            // Connect to the given socket to receive data
            dataReceiver.Connect(txtIpAddress.Text, int.Parse(txtPort.Text));

            // Start listening for stock data
            listeningThread = new Thread(new ThreadStart(dataReceiver.Listen));
            listeningThread.Start();

            // Register event handlers
            dataReceiver.OnStockReceived += new StockReceivedHandler(UpdateCurrentStocks);
            dataReceiver.OnStockReceived += new StockReceivedHandler(UpdateSummary);

            // Don't let the user try to re-connect while a connection exists
            grpConnection.Enabled = false;
        }

        /// <summary>
        /// Update the given stock using the data received.
        /// </summary>
        /// <param name="stock">Details of the stock to update.</param>
        public void UpdateCurrentStocks(StockDetails stock)
        {
            // Make sure we perform the update on the correct thread.
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateStockDelegate(UpdateCurrentStocks), new object[] { stock });
            }
            else
            {
                dgvStockPrices.Rows[stock.StockId].Cells["colPrice"].Value = string.Format("{0:0.00}", stock.StockPrice);
                dgvStockPrices.Rows[stock.StockId].Cells["colChange"].Value = string.Format("{0:0.00}", stock.StockChange);
            }
        }

        /// <summary>
        /// Updates the connection summary information with number of 
        /// updates received and the last time an update was received.
        /// </summary>
        /// <param name="stock">StockDetails object</param>
        public void UpdateSummary(StockDetails stock)
        {
            // Make sure we perform the update on the correct thread.
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateStockDelegate(UpdateSummary), new object[] { stock });
            }
            else
            {
                int noOfUpdates = int.Parse(lblUpdateCount.Text);
                ++noOfUpdates;
                lblUpdateCount.Text = noOfUpdates.ToString();
                lblLastUpdate.Text = stock.Time.ToLongTimeString();
            }
        }

        /// <summary>
        /// Try to abort the listening thread when the form closes.
        /// </summary>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            listeningThread?.Abort();
        }
    }
}
