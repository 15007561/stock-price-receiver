﻿namespace Stock_Price_Receiver
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnect = new System.Windows.Forms.Button();
            this.grpCurrentPrices = new System.Windows.Forms.GroupBox();
            this.dgvStockPrices = new System.Windows.Forms.DataGridView();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colChange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpConnectionSummary = new System.Windows.Forms.GroupBox();
            this.lblLastUpdate = new System.Windows.Forms.Label();
            this.lblTimeOfLastUpdate = new System.Windows.Forms.Label();
            this.lblUpdateCount = new System.Windows.Forms.Label();
            this.lblNoOfUpdatesReceived = new System.Windows.Forms.Label();
            this.grpConnection = new System.Windows.Forms.GroupBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.txtIpAddress = new System.Windows.Forms.TextBox();
            this.lblPort = new System.Windows.Forms.Label();
            this.lblIpAddress = new System.Windows.Forms.Label();
            this.grpCurrentPrices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockPrices)).BeginInit();
            this.grpConnectionSummary.SuspendLayout();
            this.grpConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(342, 33);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // grpCurrentPrices
            // 
            this.grpCurrentPrices.Controls.Add(this.dgvStockPrices);
            this.grpCurrentPrices.Location = new System.Drawing.Point(12, 91);
            this.grpCurrentPrices.Name = "grpCurrentPrices";
            this.grpCurrentPrices.Size = new System.Drawing.Size(446, 219);
            this.grpCurrentPrices.TabIndex = 4;
            this.grpCurrentPrices.TabStop = false;
            this.grpCurrentPrices.Text = "Current Prices";
            // 
            // dgvStockPrices
            // 
            this.dgvStockPrices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStockPrices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colName,
            this.colPrice,
            this.colChange});
            this.dgvStockPrices.Location = new System.Drawing.Point(6, 19);
            this.dgvStockPrices.Name = "dgvStockPrices";
            this.dgvStockPrices.ReadOnly = true;
            this.dgvStockPrices.Size = new System.Drawing.Size(428, 193);
            this.dgvStockPrices.TabIndex = 0;
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 125;
            // 
            // colPrice
            // 
            this.colPrice.HeaderText = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.ReadOnly = true;
            this.colPrice.Width = 125;
            // 
            // colChange
            // 
            this.colChange.HeaderText = "Change";
            this.colChange.Name = "colChange";
            this.colChange.ReadOnly = true;
            this.colChange.Width = 125;
            // 
            // grpConnectionSummary
            // 
            this.grpConnectionSummary.Controls.Add(this.lblLastUpdate);
            this.grpConnectionSummary.Controls.Add(this.lblTimeOfLastUpdate);
            this.grpConnectionSummary.Controls.Add(this.lblUpdateCount);
            this.grpConnectionSummary.Controls.Add(this.lblNoOfUpdatesReceived);
            this.grpConnectionSummary.Location = new System.Drawing.Point(12, 316);
            this.grpConnectionSummary.Name = "grpConnectionSummary";
            this.grpConnectionSummary.Size = new System.Drawing.Size(446, 71);
            this.grpConnectionSummary.TabIndex = 5;
            this.grpConnectionSummary.TabStop = false;
            this.grpConnectionSummary.Text = "Connection Summary";
            // 
            // lblLastUpdate
            // 
            this.lblLastUpdate.AutoSize = true;
            this.lblLastUpdate.Location = new System.Drawing.Point(362, 28);
            this.lblLastUpdate.Name = "lblLastUpdate";
            this.lblLastUpdate.Size = new System.Drawing.Size(33, 13);
            this.lblLastUpdate.TabIndex = 3;
            this.lblLastUpdate.Text = "None";
            // 
            // lblTimeOfLastUpdate
            // 
            this.lblTimeOfLastUpdate.AutoSize = true;
            this.lblTimeOfLastUpdate.Location = new System.Drawing.Point(249, 28);
            this.lblTimeOfLastUpdate.Name = "lblTimeOfLastUpdate";
            this.lblTimeOfLastUpdate.Size = new System.Drawing.Size(106, 13);
            this.lblTimeOfLastUpdate.TabIndex = 2;
            this.lblTimeOfLastUpdate.Text = "Time of Last Update:";
            // 
            // lblUpdateCount
            // 
            this.lblUpdateCount.AutoSize = true;
            this.lblUpdateCount.Location = new System.Drawing.Point(174, 28);
            this.lblUpdateCount.Name = "lblUpdateCount";
            this.lblUpdateCount.Size = new System.Drawing.Size(13, 13);
            this.lblUpdateCount.TabIndex = 1;
            this.lblUpdateCount.Text = "0";
            // 
            // lblNoOfUpdatesReceived
            // 
            this.lblNoOfUpdatesReceived.AutoSize = true;
            this.lblNoOfUpdatesReceived.Location = new System.Drawing.Point(17, 28);
            this.lblNoOfUpdatesReceived.Name = "lblNoOfUpdatesReceived";
            this.lblNoOfUpdatesReceived.Size = new System.Drawing.Size(151, 13);
            this.lblNoOfUpdatesReceived.TabIndex = 0;
            this.lblNoOfUpdatesReceived.Text = "Number of Updates Received:";
            // 
            // grpConnection
            // 
            this.grpConnection.Controls.Add(this.txtPort);
            this.grpConnection.Controls.Add(this.txtIpAddress);
            this.grpConnection.Controls.Add(this.lblPort);
            this.grpConnection.Controls.Add(this.lblIpAddress);
            this.grpConnection.Controls.Add(this.btnConnect);
            this.grpConnection.Location = new System.Drawing.Point(12, 12);
            this.grpConnection.Name = "grpConnection";
            this.grpConnection.Size = new System.Drawing.Size(446, 73);
            this.grpConnection.TabIndex = 6;
            this.grpConnection.TabStop = false;
            this.grpConnection.Text = "Connection";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(163, 35);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(100, 20);
            this.txtPort.TabIndex = 6;
            this.txtPort.Text = "9999";
            // 
            // txtIpAddress
            // 
            this.txtIpAddress.Location = new System.Drawing.Point(20, 35);
            this.txtIpAddress.Name = "txtIpAddress";
            this.txtIpAddress.Size = new System.Drawing.Size(100, 20);
            this.txtIpAddress.TabIndex = 5;
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(169, 18);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(32, 13);
            this.lblPort.TabIndex = 4;
            this.lblPort.Text = "Port: ";
            // 
            // lblIpAddress
            // 
            this.lblIpAddress.AutoSize = true;
            this.lblIpAddress.Location = new System.Drawing.Point(17, 18);
            this.lblIpAddress.Name = "lblIpAddress";
            this.lblIpAddress.Size = new System.Drawing.Size(64, 13);
            this.lblIpAddress.TabIndex = 3;
            this.lblIpAddress.Text = "IP Address: ";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 400);
            this.Controls.Add(this.grpConnection);
            this.Controls.Add(this.grpConnectionSummary);
            this.Controls.Add(this.grpCurrentPrices);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Stock Price Receiver";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.grpCurrentPrices.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockPrices)).EndInit();
            this.grpConnectionSummary.ResumeLayout(false);
            this.grpConnectionSummary.PerformLayout();
            this.grpConnection.ResumeLayout(false);
            this.grpConnection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.GroupBox grpCurrentPrices;
        private System.Windows.Forms.DataGridView dgvStockPrices;
        private System.Windows.Forms.GroupBox grpConnectionSummary;
        private System.Windows.Forms.Label lblUpdateCount;
        private System.Windows.Forms.Label lblNoOfUpdatesReceived;
        private System.Windows.Forms.Label lblLastUpdate;
        private System.Windows.Forms.Label lblTimeOfLastUpdate;
        private System.Windows.Forms.GroupBox grpConnection;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtIpAddress;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblIpAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChange;
    }
}

