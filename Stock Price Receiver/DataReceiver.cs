﻿// Stock Price Receiver
// Student: 15007561
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web.Script.Serialization;

namespace Stock_Price_Receiver
{
    public delegate void StockReceivedHandler(StockDetails stockDetails);

    public struct StockDetails
    {
        public int StockId;
        public string StockName;
        public double StockPrice;
        public double StockChange;
        public DateTime Time;
    }

    public class DataReceiver
    {
        // public event used to know when a stock update has been received
        public event StockReceivedHandler OnStockReceived;
        private TcpClient m_tcpClient;
        private IPAddress m_ipAddress;
        private int m_port;

        /// <summary>
        /// Constructor
        /// </summary>
        public DataReceiver()
        {
            m_tcpClient = null;
            m_ipAddress = null;
            m_port = 0;
        }

        /// <summary>
        /// Destructor, will close connection if one exists.
        /// </summary>
        ~DataReceiver()
        {
            if (m_tcpClient != null)
                m_tcpClient.Close();
        }

        /// <summary>
        /// Connects to the data generator via the given
        /// IP address and port.
        /// </summary>
        /// <param name="ipAddress">The IP address to connect to. Assumes it's valid.</param>
        /// <param name="port">Port to use connecting to the IP. Assumes it's valid.</param>
        public void Connect(string ipAddress, int port)
        {
            m_ipAddress = IPAddress.Parse(ipAddress);
            m_port = port;

            m_tcpClient = new TcpClient();
            m_tcpClient.Connect(m_ipAddress, m_port);
        }

        /// <summary>
        /// Listens for stock updates and sends the data to registered members.
        /// </summary>
        public void Listen()
        {
            const int BUFFER_SIZE = 256;
            NetworkStream stream = m_tcpClient.GetStream();
            byte[] buffer = new byte[BUFFER_SIZE];

            while (true)
            {
                // Reads the data received and serializes it into a StockDetails object.
                int numBytes = stream.Read(buffer, 0, BUFFER_SIZE);
                string message = Encoding.ASCII.GetString(buffer, 0, numBytes);
                JavaScriptSerializer serlializer = new JavaScriptSerializer();
                StockDetails stockDetails = serlializer.Deserialize<StockDetails>(message);

                // If there are register members, send them the stock details.
                if (OnStockReceived != null)
                    OnStockReceived?.Invoke(stockDetails);
            }
        }
    } // DataReceiver
}
